apt update
curl -sL https://deb.nodesource.com/setup_15.x | bash -
apt install -y nodejs
cd /vagrant
su vagrant -c "mkdir -p ~/node_modules && npm init -y fastify && npm install"
FASTIFY_ADDRESS=0.0.0.0 npm run dev
